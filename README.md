# Binance chart trader
This is a tool for setting your orders on Binance crypto exchange. Setting your price level for orders on chart is far more intuitive and quicker than typing, which is why this tool was created. Supports only Binance USDT Perpetual futures contracts. Runs on Node.js with Electron.

Includes slightly modified [klinecharts](https://www.npmjs.com/package/klinecharts) and [node-binance-api](https://www.npmjs.com/package/node-binance-api)

Prerequisites:
- You'll need Binance account and API keys 
- Fill your API key and secret inside keys.txt

```
npm i
npm start
```

Usage:
- Click entry button and set your price level on chart. Repeat with target and stop
- Position size will be calculated based on how much you are willing to lose (fees not included)
- To enter long position set entry level above stop and below target
- To enter short position set entry level below stop and above target
- When entering long and entry level is above market price a stop-market order is created. Vice versa with shorting
- Positions with P/L will appear below execution button. Middle click to close position and clear orders
- Price lines can be dragged to readjust your orders

![](./front.png)

Use at your own risk!
