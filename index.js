const trader = require('./trade.js')

var chart;
let mousedown = false
let creatingShape
let lastShapeId
color = {limit: "#089903", stop: "#d60000", target: "#d6d300"}

window.onload = function () {
    fillTimeSelector()
    fillCoinSelector()
    chart = klinecharts.init('chart')
    chart.createTechnicalIndicator('VOL')
    chart.setStyleOptions({
        candle: {
          tooltip: {
            labels: ['T: ', 'O: ', 'C: ', 'H: ', 'L: ', 'V: ']
          }
        }
      })
	
    document.getElementById("chart").addEventListener("mousedown", function(e) {
        mousedown = true   
    });
    document.getElementById("chart").addEventListener("mouseup", function(e) {
        mousedown = false
        
        if(creatingShape && lastShapeId){
            let obj = chart._chartPane._chartStore._shapeStore.getInstance(lastShapeId)
            creatingShape = false
            lastShapeId = undefined

            kolikkolista[valitut.kolikko].lines[obj._data] = obj
        }
        setLinePrice(true)
        calculatePercentages()
    });
    document.getElementById("chart").addEventListener("mousemove", function(e) {
        if(mousedown){
            setLinePrice()
            calculatePercentages()
        }
    });
    
    fillCandles()
    trader.start()
}
function setLinePrice(muunna){
    Object.values(kolikkolista[valitut.kolikko].lines).forEach(x => {
        if(x._data){
            document.getElementById(x._data).value = x._points[0].value.toFixed(kolikkolista[valitut.kolikko].pricePrecision)
            if(muunna)
                trader.setLine(x, valitut.kolikko)
        }
    })
}
function setChartPrecision(decimals){
    chart._chartPane._chartStore._pricePrecision = decimals
}
function candleStream(symbol, cb){
    trader.candleStream(symbol, cb)
}
function aggTradeStream(symbol, cb){
    trader.aggTradeStream(symbol, cb)
}

async function fillCandles(){
    if(!valitut.aika.endsWith('s'))
        chart.applyNewData(await getMinuteCandles())
    else
        fillSecondCandles()

}
function paivittaja(lista){
    chart.updateData(lista)
}
function calculatePercentages(){
    let limit = document.getElementById("entry").value
    let target = document.getElementById("target").value
    let stop = document.getElementById("stop").value
    let button = document.getElementById("enter")
    
    if(limit > target && stop > limit){
        let winpr = ((limit*0.9998)/(target*1.0002)-1)*100
        let losspr = ((limit*0.9998)/(stop*1.0004)-1)*100
        document.getElementById("targetPr").innerText = winpr.toFixed(2)+"%"
        document.getElementById("stopPr").innerText = losspr.toFixed(2)+"%"
        button.innerText = 'Short'
        button.disabled = false
        button.style = "min-width: 50px;background-color: red; font-size:18px"
        calculatePL(limit,target,stop,winpr,losspr)
    }
    else if(target > limit && limit > stop){
        let winpr = ((target*0.9998)/(limit*1.0002)-1)*100
        let losspr = ((stop*0.9996)/(limit*1.0002)-1)*100
        document.getElementById("targetPr").innerText = winpr.toFixed(2)+"%"
        document.getElementById("stopPr").innerText = losspr.toFixed(2)+"%"
        button.innerText = "Long"
        button.disabled = false
        button.style = "min-width: 50px;background-color: limegreen; font-size:18px"
        calculatePL(limit,target,stop,winpr,losspr)
    }else{
        button.disabled = true
        button.innerText = "-"
        button.style = "min-width: 50px;background-color: #8a8a8a; font-size:18px"
    }
    

}
function calculatePL(limit,target,stop,win,loss){
    //win -= 0.02
    //loss -= 0.04
    let makerfee = 0.0002
    let takerfee = 0.0004
    let fee = 0.000//3
    let maxloss = Number(document.getElementById("maxloss").value) //+ Number(document.getElementById("maxloss").value)/loss*0.06

    let lev = maxloss / loss
    let raha = Number(document.getElementById("raha").innerText)
    
    win = win / 100  + 1
    loss = loss / 100  + 1
    
    let alkumaara = raha * lev
    let voittomaara = alkumaara * win
    let tappiomaara = alkumaara * loss
    
    let voittofeemaara = alkumaara * fee + voittomaara * fee
    let tappiofeemaara = alkumaara * fee + tappiomaara * fee

    voittomaara = voittomaara - alkumaara - voittofeemaara
    tappiomaara = tappiomaara - alkumaara - tappiofeemaara
    
    document.getElementById("targetPr").innerText = document.getElementById("targetPr").innerText + " / $" + voittomaara.toFixed(2)
    document.getElementById("stopPr").innerText = document.getElementById("stopPr").innerText + " / $" + tappiomaara.toFixed(2)
    document.getElementById("lev").innerText = lev.toFixed(2)//+"x"
    document.getElementById("maara").innerText = "$"+(Number((alkumaara / limit).toFixed(kolikkolista[valitut.kolikko].quantityPrecision)) * limit).toFixed(2)
}
  
function execute(){
    trader.execute(kolikkolista[valitut.kolikko].price)
}
function positioPrint(price, symbol) {
    trader.positioPrint(price, symbol)
}
function deleteLines(symbol){
    if(kolikkolista[symbol].lines){
        if(kolikkolista[symbol].lines.limit) chart.removeShape(kolikkolista[symbol].lines.limit._id)
        if(kolikkolista[symbol].lines.target) chart.removeShape(kolikkolista[symbol].lines.target._id)
        if(kolikkolista[symbol].lines.stop) chart.removeShape(kolikkolista[symbol].lines.stop._id)
    }
}
function drawLines(symbol){
    if(kolikkolista[symbol].lines){
        if(kolikkolista[symbol].lines.limit) 
            chart._chartPane._chartStore._shapeStore.addInstance(kolikkolista[symbol].lines.limit, "candle_pane")
        if(kolikkolista[symbol].lines.target) 
            chart._chartPane._chartStore._shapeStore.addInstance(kolikkolista[symbol].lines.target, "candle_pane")
        if(kolikkolista[symbol].lines.stop) 
            chart._chartPane._chartStore._shapeStore.addInstance(kolikkolista[symbol].lines.stop, "candle_pane")
    }
}
function createLine(e){
    creatingShape = true
    if(kolikkolista[valitut.kolikko].lines && kolikkolista[valitut.kolikko].lines[e.target.innerText] && kolikkolista[valitut.kolikko].lines[e.target.innerText]._id)
    {   let obj = chart._chartPane._chartStore._shapeStore.getInstance(kolikkolista[valitut.kolikko].lines[e.target.innerText]._id)
        if(obj)
         chart.removeShape(kolikkolista[valitut.kolikko].lines[e.target.innerText]._id)
    }	//horizontalStraightLine//priceLine
    lastShapeId = chart.createShape({ key: "priceLine", data: e.target.innerText })
}
function muteBox(){
    let box = {unmute: "\ue050", mute: "\ue04f"}
	document.getElementById("mutecheck").checked ? document.getElementById("muteicon").innerText = box.mute : document.getElementById("muteicon").innerText = box.unmute
}