let lista = []
let kolikkolista = {}
let valitut = {aika:'1m', kolikko: 'BTCUSDT'}
let subscribedKline = []

function getMinuteCandles(){
	return new Promise((resolve,reject) => {
		fetch('https://www.binance.com/fapi/v1/klines?symbol='+valitut.kolikko+'&interval='+valitut.aika+'&limit=1000')
			.then(response => response.json())
			.then(data => {
				lista = data
				//paivita()
				if(!subscribedKline.includes(valitut.kolikko)){
					subscribedKline.push(valitut.kolikko)
					candleStream((valitut.kolikko).toLowerCase(), (data) => {
						futuresKline(data)
					})
				}
			  	return resolve (data.map(data => {
					return {
						timestamp: data[0],
						open: +data[1],
						high: +data[2],
						low: +data[3],
						close: +data[4],
						volume: +data[5],
					}
				})
			)			
		});
	})
}
function getAggTrades(start, end, id){
	return new Promise((resolve,reject) => {
		fetch('https://www.binance.com/fapi/v1/aggTrades?symbol='+valitut.kolikko+'&startTime='+start+'&endTime='+end)
			.then(response => response.json())
			.then(data => {
				let agglista = []
				for(let o of data){
					agglista.push({
						timestamp: o.T,
						price: +o.p,
						amount: +o.q,
						id: o.a
					})
				}
			  	return resolve (agglista)			
		});
	})
}

function futuresKline(data){
	if(data.s == valitut.kolikko){
		if(data.k.t - lista[lista.length-1][0] == lista[1][0] - lista[0][0]){
			lista.push(
				[data.k.t, data.k.o, data.k.h, data.k.l, data.k.c, data.kv]
			)
		}
		else{
			if(data.k.l < lista[lista.length-1][3])
				lista[lista.length-1][3] = data.k.l
			if(data.k.h > lista[lista.length-1][2])
				lista[lista.length-1][2] = data.k.h
			lista[lista.length-1][4] = data.k.c
			lista[lista.length-1][5] = data.k.v //1min vola
		}
		if(!valitut.aika.endsWith('s')){
			paivittaja({
				timestamp: lista[lista.length-1][0],
				open: +lista[lista.length-1][1],
				high: +lista[lista.length-1][2],
				low: +lista[lista.length-1][3],
				close: +lista[lista.length-1][4],
				volume: +lista[lista.length-1][5]
			})
		}
	}
	kolikkolista[data.s].price = +data.k.c
	positioPrint(+lista[lista.length-1][4], data.s)
}
function collectOldTrades(data){
	return new Promise((resolve,reject) => {
		let loppu = Date.now()
		let alku = loppu - 15 * 60000
		
		let taulu = []
		let initialTime = loppu-alku
		const kerailija = async function(){
			document.getElementById("loading").innerText = Math.round((1-(loppu-alku)/initialTime)*100)+"%"
			
			let arr = await getAggTrades(alku, loppu)

			alku = arr[arr.length-1].timestamp
			taulu = taulu.concat(arr)

			if(arr.length < 500){
				document.getElementById("loading").innerText = ""
				return resolve(taulu)
			}
			else{
				kerailija()
			}
				
		}
		kerailija()
	})
	
}
async function fillSecondCandles(){
	if(!kolikkolista[valitut.kolikko].secondCandleList){
		if(!kolikkolista[valitut.kolikko].aggTrades)
			kolikkolista[valitut.kolikko].aggTrades = []
		aggTradeStream(valitut.kolikko, (data) => {
			aggTrade(data)
		})
		let taulu = await collectOldTrades()
		createSecondCandles(taulu.concat(kolikkolista[valitut.kolikko].aggTrades), valitut.kolikko)
		fillSecondCandles()
	}else{
		let aikavali = +valitut.aika.split('s')[0] * 1000
		kolikkolista[valitut.kolikko].aikavali = aikavali
		if(aikavali == 1000){
			chart.applyNewData(kolikkolista[valitut.kolikko].secondCandleList)
			kolikkolista[valitut.kolikko].secondChart = kolikkolista[valitut.kolikko].secondCandleList
		}
		else{
			let currentCandle = kolikkolista[valitut.kolikko].secondCandleList[0]
			let arr = []
			for(let candle of kolikkolista[valitut.kolikko].secondCandleList){
				if(candle.timestamp % aikavali == 0){
					arr.push(currentCandle)
					currentCandle = candle
				}else{
					if(candle.high > currentCandle.high) currentCandle.high = candle.high
					if(candle.low < currentCandle.low) currentCandle.low = candle.low
					currentCandle.volume += candle.volume
					currentCandle.close = candle.close
				}
			}
			kolikkolista[valitut.kolikko].secondChart = arr
			chart.applyNewData(arr)
		}
	}
}
function createSecondCandles(taulu, symbol){
	let currentCandle = {timestamp: Math.floor(taulu[0].timestamp/1000)*1000, open: taulu[0].price, high: taulu[0].price, low: taulu[0].price, close: taulu[0].price, volume: taulu[0].amount}
	kolikkolista[symbol].secondCandleList = []
	for(let o of taulu){
		if(Math.floor(o.timestamp/1000)*1000 > currentCandle.timestamp){
			kolikkolista[symbol].secondCandleList.push(currentCandle)
			currentCandle = {timestamp: Math.floor(o.timestamp/1000)*1000, open: o.price, high: o.price, low: o.price, close: o.price, volume: o.amount}
		}
		else{
			if(currentCandle.high < o.price) currentCandle.high = o.price
			if(currentCandle.low > o.price) currentCandle.low = o.price
			currentCandle.volume  += o.amount
			currentCandle.close = o.price
		}
	}
}
function updateSecondList(data){
	let currentCandle = kolikkolista[data.symbol].secondCandleList[kolikkolista[data.symbol].secondCandleList.length-1]
	if(Math.floor(data.timestamp/1000)*1000 > currentCandle.timestamp){
		kolikkolista[data.symbol].secondCandleList.push({timestamp: Math.floor(data.timestamp/1000)*1000, open: +data.price, high: +data.price, low: +data.price, close: +data.price, volume: +data.amount})
		if(kolikkolista[data.symbol].secondCandleList.length > 10000) kolikkolista[data.symbol].secondCandleList.shift()
	}else{
		if(currentCandle.high < +data.price) currentCandle.high = +data.price
		if(currentCandle.low > +data.price) currentCandle.low = +data.price
		currentCandle.volume += +data.amount
		currentCandle.close = +data.price
	}
}
function updateSecondChart(data){
	let currentCandle = kolikkolista[data.symbol].secondChart[kolikkolista[data.symbol].secondChart.length-1]
	
	if(Math.floor(data.timestamp/kolikkolista[data.symbol].aikavali)*kolikkolista[data.symbol].aikavali > currentCandle.timestamp){
		kolikkolista[data.symbol].secondChart.push({timestamp: Math.floor(data.timestamp/kolikkolista[data.symbol].aikavali)*kolikkolista[data.symbol].aikavali, open: +data.price, high: +data.price, low: +data.price, close: +data.price, volume: +data.amount})
		if(kolikkolista[data.symbol].secondChart.length > 10000) kolikkolista[data.symbol].secondChart.shift()
		currentCandle = kolikkolista[data.symbol].secondChart[kolikkolista[data.symbol].secondChart.length-1]
	}else{
		if(currentCandle.high < +data.price) currentCandle.high = +data.price
		if(currentCandle.low > +data.price) currentCandle.low = +data.price
		currentCandle.volume += +data.amount
		currentCandle.close = +data.price
	}
	paivittaja({
		timestamp: currentCandle.timestamp,
		open: currentCandle.open,
		high: currentCandle.high,
		low: currentCandle.low,
		close: currentCandle.close,
		volume: currentCandle.volume
	})
}
function aggTrade(data){

	if(!kolikkolista[data.symbol].secondCandleList){
		kolikkolista[data.symbol].aggTrades.push(data)

		return
	}
	updateSecondList(data)
	if(data.symbol == valitut.kolikko && valitut.aika.endsWith('s')){
		updateSecondChart(data)
	}
}

function getLista(){
	return {
		timestamp: lista[lista.length-1][0],
		open: +lista[lista.length-1][1],
		high: +lista[lista.length-1][2],
		low: +lista[lista.length-1][3],
		close: +lista[lista.length-1][4],
		volume: +lista[lista.length-1][5],
	}
}

function getCoins(){
	return new Promise((resolve, reject) => {
		fetch('https://www.binance.com/fapi/v1/exchangeInfo')
		  .then(response => response.json())
		  .then(data => {
				for(let o of data.symbols){
					if(o.status == "TRADING" && o.quoteAsset == "USDT" && o.contractType == "PERPETUAL"){
						kolikkolista[o.symbol] = o
						kolikkolista[o.symbol].lines = {entry: 0, target: 0, stop: 0}
						kolikkolista[o.symbol].trade = {}
					}
				}
				return resolve(Object.keys(kolikkolista))
		});
	})
}

async function fillCoinSelector(){
	let kolikot = await getCoins()
	kolikot.sort((a,b) => {return a > b ? 1 : -1})
	let valikko = document.getElementById("kolikkovalikko")
	for(let i in kolikot){
		let t = document.createElement("option")
		if(kolikot[i] == valitut.kolikko)
			t.selected = true
		t.text = kolikot[i]
		valikko.appendChild(t)
	}
	setChartPrecision(kolikkolista[valitut.kolikko].pricePrecision)
}

let aikalista = ['1s','5s','15s','30s','1m','3m','5m','15m','30m','1h','2h','4h','6h','8h','12h','1d','3d','1w']
function fillTimeSelector(){
	let valikko = document.getElementById("aikavalikko")
	for(let i in aikalista){
		let t = document.createElement("option")
		if(aikalista[i] == valitut.aika)
			t.selected = true
		t.text = aikalista[i]
		valikko.appendChild(t)
	}
}
function changeTime(e){
	valitut.aika = e.value
	fillCandles()
}
function changeCoin(e){
	deleteLines(valitut.kolikko)
	valitut.kolikko = e.value
	setChartPrecision(kolikkolista[valitut.kolikko].pricePrecision)
	fillCandles()
	drawLines(valitut.kolikko)
}