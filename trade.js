const resti = require('./rest')
const orderfillsound = new Audio("order_filled.mp3")

const execute = function(){
	let price = kolikkolista[valitut.kolikko].price
	let trade = {}
	kolikkolista[valitut.kolikko].trade = trade
	console.log("trader, exec",price,trade)

	trade.symbol = valitut.kolikko
	trade.entry = document.getElementById("entry").value
	trade.target = document.getElementById("target").value
	trade.stop = document.getElementById("stop").value
	
	let maara = Number(document.getElementById("raha").innerText) * Number(document.getElementById("lev").innerText)
	maara = (maara / trade.entry).toFixed(kolikkolista[valitut.kolikko].quantityPrecision)

	trade.entryside = trade.entry < trade.target ? "BUY" : "SELL"
	trade.exitside = trade.entry > trade.target ? "BUY" : "SELL"

	let type = "LIMIT"
	if(trade.entryside == "BUY" && price < trade.entry || trade.entryside == "SELL" && price > trade.entry){
		type = "STOP_MARKET"
	}
	let symbol = trade.symbol
	if(!document.getElementById(symbol)){
		let label = document.createElement("label")
		label.setAttribute("id",trade.symbol)
		label.innerText = trade.symbol+" 0"
		label.onmousedown = (e) => {
			if(e.button == 1){
				resti.closePosition(symbol)
				document.getElementById(symbol).remove()
			}
		}
		document.getElementById("positiondiv").appendChild(label)
	}

	resti.order(trade.symbol, maara, trade.entry, trade.entryside, type, false)

}
module.exports.execute = execute
module.exports.start = function(){
	setBalance()
	resti.userDataFuture(data => {
		userData(data)
	})
}
async function setBalance() {
	await resti.futuresBalance().then(e => {document.getElementById("raha").innerText = (Number(e.balance)).toFixed(2)}).catch(e => {console.log(e)})
}
module.exports.positioPrint = function(price, symbol) {
	let trade = kolikkolista[symbol].trade
	let element = document.getElementById(symbol)
	if(element && trade.position){
		let pl = kolikkolista[symbol].price * trade.position.amount - trade.position.price * trade.position.amount
		element.innerText = symbol+" $"+pl.toFixed(2)
	}
}
function userData(data){
	let trade = kolikkolista[data.symbol].trade
	if(data.event == "order_update"){
		if(data.status == "FILLED" && !document.getElementById("mutecheck").checked)
			orderfillsound.play()
		if(data.side == trade.entryside){
			trade[trade.entryside] = data
			if(data.status == "FILLED")
				resti.order(trade.symbol, data.executedQty, trade.stop, trade.exitside, "STOP_MARKET", true)
		}
		else{
			if(data.type == "LIMIT")
				trade[trade.exitside] = data
			else
				trade.stopOrder = data
		}
	}
	else if(data.event == "position_update"){
		trade.position = data
		clearTimeout(trade.positionCheckAjastin)
		trade.positionCheckAjastin = positionCheck(data.symbol)
	}
}

function positionCheck(symbol){
	positionCheckAjastin = setTimeout(async() => {
		let trade = kolikkolista[symbol].trade
		if(trade.position.amount == 0){
			setBalance()
			if(document.getElementById(symbol))
				document.getElementById(symbol).remove()
			/*Object.values(liput).forEach(x => {
				chart.removeShape(liput[x])
			})*/
			resti.cancelAllFuturesOrders(trade.symbol).then(() => {}).catch(e => console.log(e))
		}
		else{
			if(!trade[trade.exitside] || Math.abs(trade.position.amount) > trade[trade.exitside].origQty){
				if(trade[trade.exitside])
					await resti.cancelOrder(trade.symbol, trade[trade.exitside].orderId).then(() => {}).catch(e => console.log(e))
				resti.order(trade.symbol, Math.abs(trade.position.amount), trade.target, trade.exitside, "LIMIT", true)
			}
				
		}
	}, 1000);
}
async function reOrder(vanha, uusi){
	let trade = kolikkolista[valitut.kolikko].trade
	console.log(vanha, uusi)
	console.log(trade[trade.entryside].origQty , trade[trade.entryside].executedQty,trade[trade.entryside].origQty - trade[trade.entryside].executedQty)
	await resti.cancelOrder(vanha.symbol, vanha.orderId).then(() => {
		resti.order(vanha.symbol, uusi.maara, uusi.hinta, vanha.side, vanha.type, uusi.exit)//.then(() => {}).catch(e => console.log(e))
	}).catch(e => {console.log(e)})
}
module.exports.candleStream = function(symbol, cb){
	resti.candleStream(symbol,cb)
}
module.exports.aggTradeStream = function(symbol, cb){
	resti.aggTradeStream(symbol,cb)
}
module.exports.setLine = async function(line, symbol){
	let trade = kolikkolista[symbol].trade

	if(trade[line._data] != document.getElementById(line._data).value){
		trade[line._data] = document.getElementById(line._data).value
		if((line._data == "entry" || line._data == "stop") && trade[trade.entryside] && trade[trade.entryside].status == "NEW"){
			resti.cancelOrder(symbol, trade[trade.entryside].orderId).then(() => {console.log("poisto");execute()}).catch(() => {})
		}
		else if(line._data == "entry" && trade[trade.entryside]){
			console.log("uusi entry")
			if(trade[trade.entryside].status == "NEW" || trade[trade.entryside].status == "PARTIALLY_FILLED"){
				reOrder({symbol: trade.symbol, orderId: trade[trade.entryside].orderId, side: trade.entryside, type: trade[trade.entryside].type}, {maara: trade[trade.entryside].origQty - trade[trade.entryside].executedQty, hinta: trade.entry, exit: false})
			}
		}
		else if(line._data == "target" && trade[trade.exitside]){
			console.log("uusi target")
			if(trade[trade.exitside].status == "NEW" || trade[trade.exitside].status == "PARTIALLY_FILLED"){
				reOrder({symbol: trade.symbol, orderId: trade[trade.exitside].orderId, side: trade.exitside, type: trade[trade.exitside].type}, {maara: trade[trade.exitside].origQty - trade[trade.exitside].executedQty, hinta: trade.target, exit: true})
			}
		}
		else if(line._data == "stop" && trade.stopOrder){
			console.log("uusi stop")
			if(trade.stopOrder.status == "NEW" || trade.stopOrder.status == "PARTIALLY_FILLED"){
				reOrder({symbol: trade.symbol, orderId: trade.stopOrder.orderId, side: trade.exitside, type: "STOP_MARKET"}, {maara: trade[trade.exitside].origQty - trade[trade.exitside].executedQty, hinta: trade.stop, exit: true})
			}
		}
	}
}
