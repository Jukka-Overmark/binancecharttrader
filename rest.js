const ws = require('ws')
const fs = require('fs')

const keys = JSON.parse(fs.readFileSync("keys.txt"))

const Binance = require('node-binance-api');
const binance = new Binance().options({
  APIKEY: keys.apiKey,
  APISECRET: keys.apiSecret
});


module.exports.userDataFuture = async function(call) {
  try{
      let key = await binance.futuresGetDataStream()
      let userws = new ws('wss://fstream.binance.com/ws/'+key.listenKey);

      let t = setInterval(async () => {
          await binance.futuresKeepDataStream()
      }, 15*60000)
      let o = setInterval(() => {
          if(userws.readyState === ws.OPEN)
              userws.ping()
          else{
            console.log("userwsf state",userws.readyState)
            clearInterval(t)
            clearInterval(o)
            userDataFuture(call)
          }
      }, 60000)

      userws.on('open', function() {
          console.log("futures userws open")
      });
      userws.on('close', async function() {
          console.log("futures userws closed")
          clearInterval(t)
          clearInterval(o)
          setTimeout(() => {
            userDataFuture(call)
          }, 5000)
      });
      userws.on('error', (e) =>{
          console.log("futures userws virhe: ",e)
      });
      userws.on('message', (message, buffer) => {    
          let data = JSON.parse(message)
          if(data.e == 'ACCOUNT_UPDATE'){
            for ( let obj of data.a.P ) {
              if(obj.ps == 'BOTH'){
                call({event:"position_update", symbol: obj.s, price: Number(obj.ep), amount: Number(obj.pa)})
              }
            }
          }
          if(data.e == 'ORDER_TRADE_UPDATE'){
            data = data.o;
            if(data.p == 0 && data.ap != 0)
              data.p = data.ap;
            let t = {event: "order_update", symbol: data.s, side: data.S, origQty: Number(data.q), price: Number(data.p), avgPrice: Number(data.ap),
              status: data.X, orderId: data.i, executedQty: Number(data.z), type: data.o, comission: 0, aika: data.T, pvm: new Date().toLocaleString()}

            call(t)
          }
      });
  }catch(err) {
      console.log("futures userdata virhe: ", err);
  }
}
module.exports.candleStream = function (symbol, cb) {
  binance.futuresSubscribe( [symbol+"@kline_1m"], (data) => {
    cb(data)
	});
}
module.exports.aggTradeStream = function (symbol, cb) {
  console.log(symbol," agg")
  binance.futuresAggTradeStream( [symbol], (data) => {
    cb(data)
	});
}
module.exports.order = async function(symbol, maara, hinta, side, type, exit){
  console.log("order",symbol, maara, hinta, side, type)
  let extra = {}
  if(exit) extra.reduceOnly = true;
  if(type && type == "STOP_MARKET"){
    extra.type = "STOP_MARKET"
    extra.stopPrice = hinta

    if(side == "BUY")
      await binance.futuresMarketBuy(symbol, maara, extra).then(() => {}).catch(e => {console.log(e)})
    else
      await binance.futuresMarketSell(symbol, maara, extra).then(() => {}).catch(e => {console.log(e)})
  }else{
    if(side == "BUY")
      await binance.futuresBuy(symbol, maara, hinta, extra).then(() => {}).catch(e => {console.log(e)})
    else
      await binance.futuresSell(symbol, maara, hinta, extra).then(() => {}).catch(e => {console.log(e)})
  }
    
}
module.exports.cancelAllFuturesOrders = async function(symbol){
  console.log("cancelAll",symbol)
  return binance.futuresCancelAll(symbol)
}
module.exports.cancelOrder = function(symbol, id){
  console.log("cancel",symbol,id)
  return binance.futuresCancel(symbol, {orderId: id})
}
module.exports.futuresBalance = () => {
  return new Promise(async (resolve,reject) => {
    await binance.futuresBalance().then(e => {resolve(e.find(x => x.asset=='USDT'))}).catch(() => reject());
  })
}

module.exports.closePosition = async function(symbol){
  const closePos = (symbol, maara) => {
    if(maara < 0) 
      return binance.futuresMarketBuy( symbol, Math.abs(maara),{reduceOnly: true})
    else 
      return binance.futuresMarketSell( symbol, maara,{reduceOnly: true})
  }

  await binance.futuresOpenOrders().then( async orders => {
    for(let o of orders){
      if(o.symbol == symbol){
        await binance.futuresCancelAll(o.symbol).then(m => console.log(o.symbol, o.side, "order poistettu")).catch(m => console.log(m))
      }
    }
  }).catch(e => {
    if(e.code && (e.code == 'ESOCKETTIMEDOUT' || e.code == 'ETIMEDOUT')){console.log('futures open pos timeout',new Date().toLocaleString())}
    else console.log(e)
  })
  
  await binance.futuresPositionRisk().then(async pos => {
    for(let o of pos){
      if(Number(o.positionAmt) != 0 && o.symbol == symbol)
        await closePos(o.symbol, Number(o.positionAmt)).then(m => console.log(o.symbol,"positio suljettu")).catch(m => console.log(m))
    }
  }).catch(e => {
    if(e.code && (e.code == 'ESOCKETTIMEDOUT' || e.code == 'ETIMEDOUT')){console.log('futures pos risk timeout',new Date().toLocaleString())}
    else console.log(e)
  })
}